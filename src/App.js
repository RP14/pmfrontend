import React, { Component } from "react";
import { Route, Switch, withRouter, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import "./SCSS/style.css";

import Auth from "./containers/auth/auth";
import Logout from "./containers/auth/logout/Logout";
import Navbar from "./components/navigation/navbar/Navbar";
import ProjectPage from "./containers/projects/ProjectPage";
import Firm from "./containers/firm/Firm";
import UserProjects from "./containers/projects/UserProjects";
import Employees from "./containers/employees/Employees";
import MainPage from "./containers/mainPage/MainPage";
import UserProfile from "./containers/user/UserProfile";
import Confirmation from "./containers/auth/confirmation/Confirmation";
import Spinner from "./components/spinner/Spinner";

class App extends Component {
  render() {
    return (
      <div>
        {this.props.isAuthenticated ? (
          <React.Fragment>
            <Navbar />
            <Switch>
              <Route path="/spinner" component={Spinner} />
              <Route path="/project/:projectId" component={ProjectPage} />
              <Route path="/firm" component={Firm} />
              <Route path="/userProjects" component={UserProjects} />
              <Route path="/userProfile" component={UserProfile} />
              <Route path="/employees" component={Employees} />
              <Route path="/logout" component={Logout} />
              <Route path="/" component={MainPage} />
              <Redirect to="/" />
            </Switch>
          </React.Fragment>
        ) : (
          <Switch>
            <Route path="/:emailAddress/:userCode" component={Confirmation} />
            <Route path="/" component={Auth} />
          </Switch>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: localStorage.getItem("token") !== null
  };
};

export default withRouter(connect(mapStateToProps)(App));
