import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import { BrowserRouter } from "react-router-dom";
import initializeApi from "../src/api/initialize";

import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import thunk from "redux-thunk";

import projectReducer from "./store/reducers/projectReducer";
import authReducer from "./store/reducers/authReducer";
import userReducer from "./store/reducers/userReducer";
import technologyReducer from "./store/reducers/technologyReducer";
import positionReducer from "./store/reducers/positionReducer";
import employeeReducer from "./store/reducers/employeeReducer";

initializeApi();
const rootReducer = combineReducers({
  projectReducer: projectReducer,
  authReducer: authReducer,
  userReducer: userReducer,
  technologyReducer: technologyReducer,
  positionReducer: positionReducer,
  employeeReducer: employeeReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
);

const app = (
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById("root"));
registerServiceWorker();
