import React, { Component } from "react";

class AddPosition extends Component {
  state = {
    positionName: "",
    alert: ""
  };

  handleChange = event => {
    this.setState({ positionName: event.target.value });
  };

  handleSubmit = event => {
    event.preventDefault();

    this.props.addPosition(this.state.positionName);
    this.setState({ alert: "Udało się utworzyć technologię." });
    this.setState({
      positionName: ""
    });
  };

  render() {
    return (
      <div className="addTechnologyBox">
        <h2 className="addTechnologyTitle">Dodaj pozycje do spisu pozycji</h2>
        <form className="AddTechnologyForm" onSubmit={this.handleSubmit}>
          <input
            className="TechnologyInputField"
            type="text"
            placeholder="Wpisz pozycje..."
            value={this.state.positionName}
            name="AddPosition"
            onChange={this.handleChange}
            required
          />
          <input className="submitButtonBrown" type="submit" value="Dodaj" />
        </form>
        {this.props.errorFetching === "fetched"
          ? [
              this.props.error !== null
                ? this.props.error.data
                : this.state.alert
            ]
          : " "}
      </div>
    );
  }
}

export default AddPosition;
