import React, { Component } from "react";
import jwt_decode from "jwt-decode";

import photoPlaceholder from "../../../assets/images/photoPlaceholder.png";

class NavbarUserProfile extends Component {
  render() {
    let token = localStorage.getItem("token");
    let decodedToken = jwt_decode(token);
    //console.log(decodedToken);

    return (
      <div className="personWhiteBar">
        <div className="personContainer">
          <span className="personName">
            <div>
              {decodedToken.given_name} {decodedToken.family_name}
            </div>
          </span>
          <span className="personFirmName">{decodedToken.email}</span>
        </div>
        <img
          className="personPhoto"
          src={decodedToken.AvatarUrl || photoPlaceholder}
          alt=""
        />
      </div>
    );
  }
}

export default NavbarUserProfile;
