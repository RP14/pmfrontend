import React, { Component } from "react";
import { Link } from "react-router-dom";
import Logo from "../../../assets/images/logo.png";
import UserPanel from "../../../containers/user/UserPanel";

class Navbar extends Component {
  state = {
    burgerClicked: false
  };

  burgerClickedHandler = () => {
    this.setState(prevState => ({
      burgerClicked: !prevState.burgerClicked
    }));
  };

  render() {
    let burgerStyle = "hamburger hamburger--emphatic";
    let navbarelementleft = "navbar-element";
    let navbarelementright = "navbar-element-right";
    let nabarelementrightwhite = "navbar-element-right navmargin";

    if (this.state.burgerClicked) {
      burgerStyle += " is-active";
      navbarelementleft += " show";
      navbarelementright += " show";
      nabarelementrightwhite += " show";
    }

    return (
      <div className="navbar">
        <Link to="/">
          <div className="navbar-element-logo">
            <img className="logo" src={Logo} alt="logo" />
          </div>
        </Link>
        <div
          className="navbar-element-hamburger"
          onClick={this.burgerClickedHandler}
        >
          <div className={burgerStyle}>
            <div className="hamburger-box">
              <div className="hamburger-inner" />
            </div>
          </div>
        </div>

        <Link to="/userProfile">
          <div className={nabarelementrightwhite}>
            <UserPanel />
          </div>
        </Link>

        <Link to="/firm">
          <div className={navbarelementleft}>
            <div className="navbar-element-text">Twoja firma</div>
          </div>
        </Link>

        <Link to="/employees">
          <div className={navbarelementleft}>
            <div className="navbar-element-text">Pracownicy</div>
          </div>
        </Link>

        <Link to="/userProjects">
          <div className={navbarelementleft}>
            <div className="navbar-element-text">Projekty</div>
          </div>
        </Link>

        <Link to="/logout">
          <div className={navbarelementright}>
            <div className="navbar-element-text">Wyloguj</div>
          </div>
        </Link>
      </div>
    );
  }
}

export default Navbar;
