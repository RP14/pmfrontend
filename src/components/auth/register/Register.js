import React from "react";

class Register extends React.Component {
  state = {
    user: {
      email: "",
      name: "",
      surname: "",
      password: "",
      confirmPassword: ""
    }
  };

  handleInputChange = event => {
    event.preventDefault();
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  };

  submitRegister = event => {
    const user = {
      email: this.state.email,
      name: this.state.name,
      surname: this.state.surname,
      password: this.state.password,
      confirmPassword: this.state.confirmPassword
    };
    event.preventDefault();
    this.props.submitRegister(user);
  };

  render() {
    return (
      <form id="loginFrom" onSubmit={this.submitRegister} autoComplete="off">
        <span className="registerInput">
          <input
            name="email"
            type="email"
            autoComplete="email"
            placeholder="Email"
            required
            onChange={event => this.handleInputChange(event)}
          />
        </span>

        <span className="registerInput">
          <input
            autoComplete="name"
            name="name"
            type="text"
            placeholder="Imię"
            required
            onChange={event => this.handleInputChange(event)}
          />
        </span>

        <span className="registerInput">
          <input
            autoComplete="surname"
            name="surname"
            type="text"
            placeholder="Nazwisko"
            required
            onChange={event => this.handleInputChange(event)}
          />
        </span>

        <span className="registerInput">
          <input
            autoComplete="password"
            name="password"
            type="password"
            placeholder="Hasło"
            required
            onChange={event => this.handleInputChange(event)}
          />
        </span>

        <span className="registerInput">
          <input
            autoComplete="confirmPassword"
            name="confirmPassword"
            type="password"
            placeholder="Powtórz hasło"
            required
            onChange={event => this.handleInputChange(event)}
          />
        </span>

        <input
          className="registerSubmitButtonBrown"
          type="submit"
          value="Załóż konto"
        />

        {this.props.response === 201 ? (
          <div>Na podany e-mail został wysłany link aktywacyjny.</div>
        ) : null}

        {this.props.error}
      </form>
    );
  }
}

export default Register;
