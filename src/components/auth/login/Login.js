import React from "react";
import { Link } from "react-router-dom";

class Login extends React.Component {
  state = {
    user: {
      email: "",
      password: ""
    }
  };

  handleInputChange(event) {
    event.preventDefault();
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });

    //this.setState({ [event.target.name]: event.target.value });
  }

  submitLogin = event => {
    const user = {
      email: this.state.email,
      password: this.state.password
    };

    event.preventDefault();
    this.props.submitLogin(user);
  };

  render() {
    return (
      <form id="loginFrom" onSubmit={this.submitLogin}>
        <span className="input">
          <input
            type="email"
            autoComplete="off"
            placeholder="Email"
            name="email"
            required
            onChange={event => this.handleInputChange(event)}
          />
        </span>
        <span className="input">
          <input
            type="password"
            autoComplete="off"
            placeholder="Hasło"
            name="password"
            required
            onChange={event => this.handleInputChange(event)}
          />
        </span>
        <Link to="" className="resetPassword">
          Zapomniałeś hasła?
        </Link>
        <input className="submitButtonBrown" type="submit" value="Zaloguj" />

        {this.props.error}
      </form>
    );
  }
}

export default Login;
