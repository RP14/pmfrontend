import React, { Component } from "react";
import photoPlaceholder from "../../assets/images/photoPlaceholder.png";

class Person extends Component {
  showEmployee = email => {
    this.props.loadEmployee(email);
  };

  handleAcceptCandidate = () => {
    const projectId = this.props.projectId;
    const email = this.props.email;
    this.props.acceptCandidate(projectId, email);
  };

  handleRemoveParticipant = () => {
    const projectId = this.props.projectId;
    const email = this.props.email;
    this.props.removeParticipant(projectId, email);
  };

  render() {
    return (
      <div className="hello">
        <img
          className="person__avatar"
          src={this.props.photoUrl || photoPlaceholder}
          alt="Av"
        />

        {this.props.type === "employees" ? (
          <div
            className="person__link"
            onClick={() => this.showEmployee(this.props.email)}
          >
            <div className="person__name">
              <b>
                {this.props.name} {this.props.surname}
              </b>
              <br />
              {this.props.email}
            </div>
            <div className="person__profile">Profil ></div>
          </div>
        ) : (
          <div className="person__name">
            <b>
              {" "}
              {this.props.name} {this.props.surname}{" "}
            </b>
            <br />
            {this.props.email}
            <br />
            {this.props.position}
            {this.props.role}
          </div>
        )}

        {this.props.type === "team" ? (
          <div className="person__status">
            {" "}
            {this.props.role === "Team Leader" ? (
              <div className="material-icons person__status TLStar">grade</div>
            ) : (
              [
                this.props.isTeamLeader ? (
                  <button
                    className="material-icons groupAdd"
                    onClick={this.handleRemoveParticipant}
                  >
                    close
                  </button>
                ) : (
                  " "
                )
              ]
            )}
          </div>
        ) : null}

        {this.props.type === "candidates" ? (
          <div className="team__status">
            <button
              className="material-icons groupAdd"
              onClick={this.handleAcceptCandidate}
            >
              group_add
            </button>
          </div>
        ) : null}
      </div>
    );
  }
}

export default Person;
