import React, { Component } from "react";
import SpinnerGif from '../../assets/images/spinner.gif';

class Spinner extends Component {
  render() {
    return (
      <div>
            <div className="catbox">
                <img src={SpinnerGif} alt="Loading..." className={this.props.type}/>
            </div>
      </div>
    );
  }
}

export default Spinner;
