import React, { Component } from "react";

class AddTechnology extends Component {
  state = {
    technologyName: "",
    alert: ""
  };

  handleChange = event => {
    this.setState({ technologyName: event.target.value });
    this.setState({ alert: "" });
  };

  handleSubmit = event => {
    event.preventDefault();

    this.props.addTechnology(this.state.technologyName);

    this.setState({ alert: "Udało się utworzyć technologię." });
    this.setState({
      technologyName: ""
    });
  };

  render() {
    return (
      <div className="addTechnologyBox">
        <h2 className="addTechnologyTitle">
          Dodaj technologie do spisu kwalifikacji
        </h2>

        <form className="AddTechnologyForm" onSubmit={this.handleSubmit}>
          <input
            className="TechnologyInputField"
            type="text"
            placeholder="Wpisz technologie..."
            value={this.state.technologyName}
            name="AddQuali"
            onChange={this.handleChange}
            required
          />

          <input className="submitButtonBrown" type="submit" value="Dodaj" />
        </form>

        {this.props.errorFetching === "fetched"
          ? [
              this.props.error !== null
                ? this.props.error.data
                : this.state.alert
            ]
          : " "}
      </div>
    );
  }
}

export default AddTechnology;
