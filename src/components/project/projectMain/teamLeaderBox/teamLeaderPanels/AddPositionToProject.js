import React, { Component } from "react";
import Select from "react-select";

class AddPositionToProject extends Component {
  state = {
    selectedPositions: []
    //alert: "",
    // showError: " "
  };

  handlePositionsChange = selectedPositions => {
    this.setState({ selectedPositions });
    this.setState({ alert: "" });
    // this.setState({ showError: false });
  };

  handleSubmit = event => {
    event.preventDefault();
    if (this.state.selectedPositions.length !== 0) {
      let positionsValues = this.state.selectedPositions.map(
        element => element.value
      );
      let projectId = this.props.projectId;
      this.props.addPositionsToProject(projectId, positionsValues);
      // this.setState({ showError: true });
      //this.setState({ alert: "Udało się utworzyć pozycje." });
    } else {
      //  this.setState({ showError: true });
      this.setState({ alert: "Pole nie moze byc puste" });
    }
  };

  render() {
    return (
      <div className="teamleader__minipanel">
        <div className="minipanel_title">Dodaj pozycje do projektu</div>
        <div className="description addposition">
          <form className="search-container" onSubmit={this.handleSubmit}>
            <Select
              placeholder="Wybierz pozycje do projektu"
              className="AddProjectSelectField positions"
              //value={this.selectedPositions}
              onChange={this.handlePositionsChange}
              options={this.props.positionsOptions}
              isMulti={true}
              isSearchable={true}
            />
            <button className="material-icons search-icon">add</button>
          </form>
          {/* {this.state.showError === true
              ? [
                  this.props.error !== null
                    ? this.props.error.data
                    : this.state.alert
                ]
              : " "} */}
          {this.state.alert}
        </div>
      </div>
    );
  }
}
export default AddPositionToProject;
