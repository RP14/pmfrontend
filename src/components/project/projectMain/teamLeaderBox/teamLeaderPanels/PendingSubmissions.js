import React, { Component } from "react";
import Person from "../../../../person/Person";

class PendingSubmissions extends Component {
  render() {
    let users = "Nie ma kandydatów.";

    if (
      this.props.projectCandidates &&
      this.props.projectCandidates.length > 0
    ) {
      users = this.props.projectCandidates.map(person => {
        return (
          <Person
            key={person.emailAddress}
            name={person.firstname}
            surname={person.lastname}
            email={person.emailAddress}
            position={person.positionName}
            photoUrl={person.photoUrl}
            projectId={this.props.projectId}
            acceptCandidate={this.props.acceptCandidate}
            type={"candidates"}
          />
        );
      });
    }

    return (
      <div className="teamleader__minipanel">
        <div className="minipanel_title">Oczekujący</div>
        <div className="center_of_site">{users}</div>
      </div>
    );
  }
}

export default PendingSubmissions;
