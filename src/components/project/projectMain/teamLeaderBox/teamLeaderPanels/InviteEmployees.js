import React, { Component } from "react";
import Select from "react-select";

class InviteEmployees extends Component {
  state = {
    selectedPositions: null
  };

  handlePositionsChange = selectedPositions => {
    this.setState({ selectedPositions });
  };

  render() {
    return (
      <div className="teamleader__minipanel">
        <div className="minipanel_title">Dodaj pracowników do projektu</div>
        <div className="description">
          <form className="search-container">
            <Select
              placeholder="Wyszukaj pracownika..."
              className="AddProjectSelectField positions"
              value={this.selectedOption}
              onChange={this.handlePositionsChange}
              options={this.props.employeesOptions}
              isMulti={false}
              isSearchable={true}
            />

            {/* <div className="searchicon">
              <div className="material-icons search-icon">search</div>
    </div> */}
          </form>
        </div>
      </div>
    );
  }
}
export default InviteEmployees;
