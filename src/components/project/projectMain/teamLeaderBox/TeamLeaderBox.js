import React, { Component } from "react";
import PendingSubmissions from "./teamLeaderPanels/PendingSubmissions";
import InviteEmployees from "./teamLeaderPanels/InviteEmployees";
import AddPositionToProject from "./teamLeaderPanels/AddPositionToProject";

class TeamLeaderBox extends Component {
  render() {
    return (
      <div className="teamleader__panel">
        <div className="project__title">Panel Team Leadera</div>
        <div className="maindescription">
          <div className="teamLeaderBars">
            <InviteEmployees employeesOptions={this.props.employeesOptions} />
            <AddPositionToProject
              positionsOptions={this.props.positionsOptions}
              addPositionsToProject={this.props.addPositionsToProject}
              projectId={this.props.projectId}
              error={this.props.error}
              errorFetching={this.props.errorFetching}
            />
            <PendingSubmissions
              projectCandidates={this.props.projectCandidates}
              projectId={this.props.projectId}
              acceptCandidate={this.props.acceptCandidate}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default TeamLeaderBox;
