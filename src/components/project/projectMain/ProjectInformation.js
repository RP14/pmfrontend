import React, { Component } from "react";

class ProjectInformation extends Component {
  render() {
    return (
      <div className="projectpage__box">
        <article className="projectpage__card">
          <div className="projectpage__card__corner">
            <div className="projectpage__card__corner-triangle" />
          </div>
          <div className="project__title">{this.props.project.name}</div>
          <div className="maindescription">
            {this.props.project.description}
          </div>
          <div className="project__title">Technologie:</div>
          <div className="description">
            {" "}
            {this.props.project.technologies
              ? this.props.project.technologies.map(projectTechnology => (
                  <div key={projectTechnology.id}>
                    {projectTechnology.technologyName}
                  </div>
                ))
              : null}
          </div>
        </article>
      </div>
    );
  }
}

export default ProjectInformation;
