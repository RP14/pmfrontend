import React, { Component } from "react";
import moment from "moment";

class ProjectWantedPanel extends Component {
  state = {
    date: "",
    time: "",
    selectedPosition: ""
  };

  componentDidMount() {
    if (this.props.recruitmentEndDate) {
      this.setState({ date: this.props.recruitmentEndDate.slice(0, 10) });
      this.setState({ time: this.props.recruitmentEndDate.slice(11, 16) });
    }
  }

  addCandidate = event => {
    const email = localStorage.getItem("email");
    const projectId = this.props.projectId;
    const positionId = this.state.selectedPosition;
    this.props.addCandidate(email, projectId, positionId);
  };

  handlePositionSelect = positionId => {
    this.setState({ selectedPosition: positionId });
  };

  render() {
    console.log("db", this.props);
    const dateAndTime = moment(this.state.date + " " + this.state.time);
    const currentDate = moment();
    const daysLeft = dateAndTime.diff(currentDate, "d");
    const hoursLeft = dateAndTime.diff(currentDate, "hours") % 24;

    return (
      <div className="project__tile2">
        <div className="members_title">Poszukiwani</div>

        <div>
          <small>
            Zapisy trwają do: {dateAndTime.format("D.MM.YYYY H:mm")}
          </small>
        </div>

        <div className="find__description">
          <ul>
            {this.props.projectPositions &&
              this.props.projectPositions.map(position => {
                return (
                  <li
                    key={position.id}
                    className="wantedPanelPosition"
                    tabIndex="-1"
                    onClick={() =>
                      this.handlePositionSelect(position.positionId)
                    }
                  >
                    {position.positionName}
                  </li>
                );
              })}
          </ul>
        </div>

        {daysLeft > 0 &&
          hoursLeft > 0 && (
            <button className="button" onClick={this.addCandidate}>
              Zgloś sie do projektu
            </button>
          )}

        <div>
          <small>
            Pozostało:{" "}
            {daysLeft < 1 ? (
              " "
            ) : daysLeft > 1 ? (
              <span>{daysLeft} dni</span>
            ) : (
              <span>{daysLeft} dzień</span>
            )}{" "}
            {hoursLeft < 1 ? (
              "Zapisy zostały zamkniete"
            ) : (
              <span>{hoursLeft} h</span>
            )}
          </small>
        </div>
      </div>
    );
  }
}

export default ProjectWantedPanel;
