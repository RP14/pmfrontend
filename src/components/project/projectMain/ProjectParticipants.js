import React, { Component } from "react";
import Person from "../../person/Person";

class ProjectParticipants extends Component {
  render() {
    let users = "Nie ma zespołu.";

    if (
      this.props.projectParticipants &&
      this.props.projectParticipants.length > 0
    ) {
      users = this.props.projectParticipants.map(person => {
        return (
          <Person
            key={person.emailAddress}
            role={person.roleName}
            name={person.firstname}
            surname={person.lastname}
            email={person.emailAddress}
            photoUrl={person.photoUrl}
            projectId={this.props.projectId}
            removeParticipant={this.props.removeParticipant}
            isTeamLeader={this.props.isTeamLeader}
            type={"team"}
          />
        );
      });
    }

    return (
      <div className="project__tile">
        <div className="members_title">Zespół</div>
        <div className="center_of_site">{users}</div>
      </div>
    );
  }
}

export default ProjectParticipants;
