import React, { Component } from "react";
import { Link } from "react-router-dom";

class ProjectTile extends Component {
  state = {};

  render() {
    return (
      <div className="box">
        <article className="card">
          <div className="card__corner">
            <div className="card__corner-triangle" />
          </div>
          <div className="projectlist__title">{this.props.name}</div>
          <div className="projecttile__description">
            {this.props.description.substring(0, 250)}
          </div>
        </article>
        <center>
          <Link to={`/project/${this.props.id}`}>
            <button className="button">wyświetl szczegóły...</button>
          </Link>
        </center>
      </div>
    );
  }
}

export default ProjectTile;
