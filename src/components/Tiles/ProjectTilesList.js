import React, { Component } from "react";
import ProjectTile from "../Tiles/projectTile/ProjectTile";

class ProjectTilesList extends Component {
  render() {
    return (
      <div className="projectstile">
        {this.props.projects.map(project => {
          return (
            <ProjectTile
              key={project.projectId}
              id={project.projectId}
              name={project.name}
              description={project.description}
            />
          );
        })}
      </div>
    );
  }
}

export default ProjectTilesList;
