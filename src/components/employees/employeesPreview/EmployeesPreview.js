import React from "react";
import PlaceholderPhoto from "../../../assets/images/photoPlaceholder.png";
import EmployeeQualiList from "../employeesPreview/EmployeeQualiList";

class EmployeesPreview extends React.Component {
  render() {
    return (
      <div className="employeePreviewProfileBox">
        <h2 className="employeePreviewPanelTitle">Profil pracownika</h2>

        {this.props.employee.length !== 0 ? (
          <div>
            <div className="employeePreviewPhotoAndInfo">
              <div className="employeePhotoBackground">
                <img
                  className="employeePhoto"
                  src={this.props.employee.photoUrl || PlaceholderPhoto}
                  alt=""
                />
              </div>
              <div className="employeeInfoBox">
                <span className="employeePanelInputsTitles">Imię</span>
                <div className="employeeInfo">
                  {this.props.employee.firstname}
                </div>

                <span className="employeePanelInputsTitles">Nazwisko</span>
                <div className="employeeInfo">
                  {this.props.employee.lastname}
                </div>

                <span className="employeePanelInputsTitles">Oddział</span>
                <div className="employeeInfo">
                  Oddział w Białymstoku ul. Szkolna 17
                </div>

                <span className="employeePanelInputsTitles">E-mail</span>
                <div className="employeeInfo">
                  {this.props.employee.emailAddress}
                </div>
              </div>
            </div>

            <div className="employeeQualiBox">
              <EmployeeQualiList
                technologies={this.props.employee.userTechnologies}
              />
            </div>
          </div>
        ) : (
          <div>Wybierz osobę, by zobaczyć jej profil.</div>
        )}
      </div>
    );
  }
}

export default EmployeesPreview;
