import React from "react";

class EmployeeQualiList extends React.Component {
  render() {
    return (
      <div className="employeerQualiContainer">
        <h2 className="employeeQualiListTitle qualiPreview">Kwalifikacje</h2>
        <ul className="employeerQualiList">
          {this.props.technologies && this.props.technologies.length > 0 ? (
            this.props.technologies.map(technology => {
              return (
                <li className="employeeQuali" key={technology.id}>
                  {technology.technologyName}
                </li>
              );
            })
          ) : (
            <div> Brak kwalifikacji. </div>
          )}
        </ul>
      </div>
    );
  }
}

export default EmployeeQualiList;
