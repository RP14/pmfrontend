import React from "react";
import Person from "../../person/Person";
import Spinner from "../../spinner/Spinner";

class EmployeesList extends React.Component {
  render() {
    let users = this.props.employees.map(person => {
      return (
        <Person
          key={person.emailAddress}
          name={person.firstname}
          surname={person.lastname}
          email={person.emailAddress}
          loadEmployee={this.props.loadEmployee}
          photoUrl={person.photoUrl}
          type={"employees"}
        />
      );
    });

    return (
      <div className="employeeListBox">
        <div className="employeeList">Lista pracowników</div>
        {this.props.loading ? (
          <Spinner />
        ) : (
          <React.Fragment>{users}</React.Fragment>
        )}
      </div>
    );
  }
}

export default EmployeesList;
