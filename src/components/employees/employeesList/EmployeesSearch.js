import React from 'react';

class EmployeesSearch extends React.Component {
  state = {
    employee: ' '
  }

  changeInputHandler = (event) => {
   
    if (event.target.value.length < 1) {
      this.props.filterEmployees(" ");
    } else if (event.target.value.length > 1) {
      this.props.filterEmployees(event.target.value);
    }
  }

  render() {
    return (
      <div className="employeesSearchBox">
        <div className="searchEmployeeTitle">Wyszukaj pracownika</div>
        <div className="searchEmployeeDesc">
          <form className="searchEmployeeContainer">
            <input type="text" id="employeeSearchBar" placeholder="Wyszukaj pracownika..." onChange={this.changeInputHandler}/>
            <div className="searchEmployeeIcon">
              <div className="material-icons search-icon">search</div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default EmployeesSearch;
