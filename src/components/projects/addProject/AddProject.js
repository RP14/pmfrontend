import React from "react";
import Select from "react-select";
import moment from "moment";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { addProject } from "../../../store/actions/projectActions";

//po submicie bedzie przenosiło na strone projektu i nie trzeba czyscic pol essa
class AddProject extends React.Component {
  state = {
    newProject: {
      name: "",
      description: "",
      startDate: "",
      endDate: "",
      status: "",
      teamLeaderEmail: "",
      teamLeaderPositionId: "",
      technologies: []
    },

    selectedOption: null,
    selectedPosition: null,
    dataInputActive: false,
    alert: ""
  };

  dateInputClicked = () => {
    this.setState({ dataInputActive: true });
  };

  handleChange = event => {
    event.preventDefault();
    this.setState({ [event.target.name]: event.target.value });
  };

  handleTechnologiesChange = selectedTechnology => {
    this.setState({ selectedTechnology });
  };

  handlePositionChange = selectedPosition => {
    this.setState({ selectedPosition });
  };

  handleSubmit = event => {
    const newProject = {
      name: this.state.name,
      description: this.state.description,
      startDate: moment().format("L"),
      endDate: this.state.endDate,
      status: "Otwarty",
      teamLeaderEmail: this.state.teamLeaderEmail,
      teamLeaderPositionId: this.state.selectedPosition.value,
      technologies: this.state.selectedTechnology.map(element => element.value)
    };
    event.preventDefault();
    this.props.addProject(newProject, this.props.history);
    this.setState({ alert: "Udało się dodać projekt." });
  };

  render() {
    return (
      <form className="addProjectBox" onSubmit={this.handleSubmit}>
        <h2 className="addTechnologyTitle">Stwórz projekt</h2>
        <div className="addProjectForm">
          <div className="projectData">
            <input
              className="AddProjectInputField"
              type="text"
              placeholder="Wpisz nazwe projektu..."
              name="name"
              onChange={this.handleChange}
              required
            />
            <input
              className="AddProjectInputField"
              type="email"
              placeholder="Wpisz e-mail lidera projektu"
              name="teamLeaderEmail"
              onChange={this.handleChange}
              required
            />
            <Select
              placeholder="Wybierz pozycje lidera projektu"
              className="AddProjectSelectField"
              value={this.selectedPosition}
              onChange={this.handlePositionChange}
              options={this.props.positionsOptions}
              isSearchable={true}
            />
            <input
              className={
                this.state.dataInputActive
                  ? "AddProjectInputField"
                  : "AddProjectInputField DatePlaceholder"
              }
              type="date"
              placeholder="Wpisz date zakończenia zapisów"
              name="endDate"
              onChange={this.handleChange}
              onClick={this.dateInputClicked}
              max="9999-12-31"
              required
            />
            <Select
              placeholder="Wybierz technologie do projektu"
              className="AddProjectSelectField"
              value={this.selectedOption}
              onChange={this.handleTechnologiesChange}
              options={this.props.technologiesOptions}
              isMulti={true}
              isSearchable={true}
            />
          </div>
          <div className="projectDesc">
            <textarea
              className="AddProjectDescField"
              placeholder="Wpisz opis projektu..."
              name="description"
              onChange={this.handleChange}
              required
            />
          </div>
        </div>
        <input className="submitButtonBrown" type="submit" value="Stwórz" />
        <span className="addProjectResponse">
          {this.props.errorFetching === "fetched"
            ? [
                this.props.error !== null
                  ? this.props.error.data
                  : this.state.alert
              ]
            : " "}
        </span>
      </form>
    );
  }
}

export default withRouter(
  connect(
    null,
    { addProject }
  )(AddProject)
);
