import React from "react";
import photoPlaceholder from "../../../assets/images/photoPlaceholder.png";

class UserInfo extends React.Component {
  state = {
    isHovering: false,
    isSelected: false,
    photoUrl: "",
    file: ""
  };

  handleMouseHoverEnter = () => {
    this.setState({ isHovering: true });
  };

  handleMouseHoverLeave = () => {
    this.setState({ isHovering: false });
  };

  handleImageSelection = () => {
    this.setState({ isSelected: true });
  };

  handleUserPhotoSelection = event => {
    event.preventDefault();
    let reader = new FileReader();
    let file = event.target.files[0];
    reader.onloadend = () => {
      this.setState({
        file: file
      });
    };
    reader.readAsDataURL(file);
  };

  handleImageUpload = event => {
    event.preventDefault();
    const userMail = this.props.user.emailAddress;
    const formDataFile = new FormData();
    formDataFile.append("file", this.state.file);
    if (formDataFile !== "") {
      console.log("handle update");
      this.props.updateUserPhoto(userMail, formDataFile); //funkcja docelowa
      this.setState({ isSelected: false });
    }
  };

  render() {
    return (
      <div
        className="photoBackground"
        onMouseEnter={this.handleMouseHoverEnter}
        onMouseLeave={this.handleMouseHoverLeave}
      >
        {this.state.isSelected || this.state.isHovering ? (
          <form className="photoUpload">
            <label className="photoUploadTitle">
              Wybierz zdjęcie to przesłania
            </label>
            <div className="uploadPhotoInputPosition">
              <input
                onClick={this.handleImageSelection}
                onChange={this.handleUserPhotoSelection}
                type="file"
                id="photoUploadInput"
              />
            </div>
            <div className="photoUploadButtons">
              <button onClick={this.handleImageUpload}>Prześlij</button>
              <button onClick={() => (this.isSelected = "false")}>
                Zaniechaj
              </button>
            </div>
          </form>
        ) : (
          <img
            className="userPhoto"
            src={this.props.user.photoUrl || photoPlaceholder}
            alt="Zdjecie profilowe"
          />
        )}
      </div>
    );
  }
}

export default UserInfo;
