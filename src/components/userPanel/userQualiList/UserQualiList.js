import React from "react";
import Spinner from "../../spinner/Spinner";

class UserQualiList extends React.Component {
  removeTechnologyHandler = (email, id) => {
    this.props.removeUserTechnology(email, id);
  };

  render() {
    let userTechnologies = null;

    if (this.props.userTechnologies) {
      userTechnologies = this.props.userTechnologies.map(userTechnology => {
        return (
          <li className="userQuali" key={userTechnology.id}>
            {userTechnology.technologyName}
            <button
              onClick={() =>
                this.removeTechnologyHandler(
                  this.props.userEmail,
                  userTechnology.id
                )
              }
              className="material-icons qualiIcons"
            >
              clear
            </button>
          </li>
        );
      });
    }

    return (
      <div className="employeeQualiContainer">
        <h2 className="employeeQualiListTitle">Twoje kwalifikacje</h2>
        {this.props.loading ? (
          <Spinner />
        ) : (
          <ul className="userQualiList">{userTechnologies}</ul>
        )}
      </div>
    );
  }
}

export default UserQualiList;
