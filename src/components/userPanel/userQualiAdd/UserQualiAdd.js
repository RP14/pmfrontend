import React from "react";
import Select from "react-select";

class UserQualiAdd extends React.Component {
  state = {
    selectedOption: null
  };

  handleTechnologiesChange = selectedOption => {
    this.setState({ selectedOption });
  };

  handleSubmit = event => {
    event.preventDefault();

    if (this.state.selectedOption) {
      this.props.addUserTechnology(
        this.props.userEmail,
        this.state.selectedOption.value
      );
      this.setState({ selectedOption: null });
    }
  };

  componentWillReceiveProps;

  render() {
    let noUserOptions = this.props.userOptions.map(option => {
      return { value: option.value, label: option.label };
    });
    return (
      <div className="userQualiBox">
        <h2 className="userQualiAddTitle">Dodaj kwalifikacje</h2>

        <form className="userQualiAddForm" onSubmit={this.handleSubmit}>
          <Select
            placeholder="Wybierz technologie..."
            className="AddProjectSelectField"
            value={this.state.selectedOption}
            onChange={this.handleTechnologiesChange}
            options={noUserOptions}
            isMulti={false}
            isSearchable={true}
          />

          <input className="submitButtonBrown" type="submit" value="Dodaj" />
        </form>
      </div>
    );
  }
}

export default UserQualiAdd;
