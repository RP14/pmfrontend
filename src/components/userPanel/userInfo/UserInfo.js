import React from "react";
import Spinner from '../../spinner/Spinner';

class UserInfo extends React.Component {
  state = {
    userNameAfterEdit: "",
    userSurnameAfterEdit: "",

    editName: false,
    editSurname: false
  };

  toggleEditInput = event => {
    this.setState({ [event]: !this.state[event] });
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  userNameSubmit = event => {
    event.preventDefault();
    const newUserName = this.state.userNameAfterEdit;
    const userMail = this.props.user.emailAddress;
    const element = "firstname";
    if (newUserName === "") {
      this.setState({ editName: false });
    } else {
      this.props.updateUserInfo(userMail, newUserName, element);
      this.setState({ editName: false });
    }
  };

  userSurnameSubmit = event => {
    event.preventDefault();
    const newUserSurname = this.state.userSurnameAfterEdit;
    const userMail = this.props.user.emailAddress;
    const element = "lastname";
    if (newUserSurname === "") {
      this.setState({ editSurname: false });
    } else {
      this.props.updateUserInfo(userMail, newUserSurname, element);
      this.setState({ editSurname: false });
    }
  };

  render() {
    return (
      <div className="userInfoBox">
      {this.props.loading ? <Spinner/> : <React.Fragment>{!this.state.editName ? (
          <React.Fragment>
            <span className="userPanelInputsTitles">Imię</span>
            <div className="userInfo">
              {this.props.user.firstname}
              <button
                onClick={() => this.toggleEditInput("editName")}
                className="material-icons profileIcons"
              >
                edit
              </button>
            </div>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <span className="userPanelInputsTitles">Edytuj imię</span>
            <form className="userInfo" onSubmit={this.userNameSubmit}>
              <input
                className="userPanelInputs"
                name="userNameAfterEdit"
                defaultValue={this.props.user.firstname}
                onChange={this.handleChange}
              />
              <button
                type="submit"
                value="Zmień"
                className="material-icons profileIcons"
              >
                done
              </button>
              <button
                onClick={() => this.toggleEditInput("editName")}
                className="material-icons profileIcons"
              >
                edit
              </button>
            </form>
          </React.Fragment>
        )}

        {!this.state.editSurname ? (
          <React.Fragment>
            <span className="userPanelInputsTitles">Nazwisko</span>
            <div className="userInfo">
              {this.props.user.lastname}
              <button
                onClick={() => this.toggleEditInput("editSurname")}
                className="material-icons profileIcons"
              >
                edit
              </button>
            </div>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <span className="userPanelInputsTitles">Edytuj nazwisko</span>
            <form className="userInfo" onSubmit={this.userSurnameSubmit}>
              <input
                className="userPanelInputs"
                name="userSurnameAfterEdit"
                defaultValue={this.props.user.lastname}
                onChange={this.handleChange}
              />
              <button
                type="submit"
                value="Zmień"
                className="material-icons profileIcons"
              >
                done
              </button>
              <button
                onClick={() => this.toggleEditInput("editSurname")}
                className="material-icons profileIcons"
                type="button"
              >
                edit
              </button>
            </form>
          </React.Fragment>
        )}
        <span className="userPanelInputsTitles">E-mail</span>
        <div className="userInfo">{this.props.user.emailAddress}</div>
</React.Fragment>}
        
        {/* {!this.state.editBranch ? (
          <React.Fragment>
            <span className="userPanelInputsTitles">Oddział</span>
            <div className="userInfo">
              {this.state.userBranch}
              <button
                onClick={() => this.toggleEditInput("editBranch")}
                className="material-icons profileIcons"
              >
                edit
              </button>
            </div>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <span className="userPanelInputsTitles">Edytuj nazwe oddziału</span>
            <form className="userInfo">
              <input
                className="userPanelInputs"
                name="userBranch"
                defaultValue={this.state.userBranch}
                onChange={this.handleChange}
              />
              <button
                onClick={() => this.toggleEditInput("editBranch")}
                className="material-icons profileIcons"
              >
                edit
              </button>
            </form>
          </React.Fragment>
        )} */}

        
      </div>
    );
  }
}

export default UserInfo;
