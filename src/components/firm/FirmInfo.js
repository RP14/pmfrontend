import React, { Component } from "react";
import TTLogo from "../../assets/images/ttms.png";

class FirmInfo extends Component {
  render() {
    return (
      <div className="urfirm__container">
        <img src={TTLogo} alt="Logo" height="200px" />
        <div className="urfirm__title">
          Transition Technologies Managed Services
        </div>
        <div className="urfirm__description">
          O dynamicznym rozwoju Transition Technologies świadczy stale rosnąca
          liczba lokalizacji firmy. Nasze biura znajdują się w Warszawie, Łodzi,
          Wrocławiu, Białymstoku, Lublinie, Kielcach, Koszalinie, Olsztynie i
          Ostrowie Wielkopolskim. Od 2003 roku zarejestrowana w Niemczech spółka
          European Challenge jest przedstawicielem Transition Technologies na
          rynku zachodnioeuropejskim, natomiast w 2007 roku w Pittsburghu swoją
          działalność rozpoczęła zależna od firmy spółka Transition Technologies
          USA Inc. Posiadamy również biura sprzedażowe w Wielkiej Brytanii i
          Szwecji.
        </div>
        <div className="urfirm__title">Działalność</div>✓ produkcja i sprzedaż
        oprogramowania dla przemysłu
        <br />✓ optymalizacja procesów technologicznych
        <br />✓ handel energią elektryczną i gazem
        <br />✓ zarządzanie ryzykiem
        <br />✓ prognozowanie
        <br />✓ usługi inżynierskie
        <br />✓ outsourcing usług programistycznych w tym rozwój i wdrożenia
        rozwiązań PLM Windchill
        <br />✓ consulting usług programistycznych
        <br />✓ szkolenia
      </div>
    );
  }
}

export default FirmInfo;
