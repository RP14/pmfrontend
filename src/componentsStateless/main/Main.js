import React from "react";
import { Switch, Route } from "react-router-dom";
import Navbar from "../../components/navigation/navbar/Navbar";
import MainPage from "../../components/content/mainPage/MainPage";
import Employees from "../../containers/employees/Employees";
import UserProfile from "../../containers/user/UserProfile";
import Firm from "../../containers/firm/Firm";
import UserProjects from "../../containers/projects/UserProjects";
import ProjectPage from "../../containers/projects/ProjectPage";

const Main = () => {
  return (
    <React.Fragment>
      <Navbar />
      <Switch>
        <Route path="/project" component={ProjectPage} />
        <Route path="/firm" component={Firm} />
        <Route path="/userProjects" component={UserProjects} />
        <Route path="/userProfile" component={UserProfile} />
        <Route path="/employees" component={Employees} />
        <Route path="/" exact component={MainPage} />
      </Switch>
    </React.Fragment>
  );
};

export default Main;
