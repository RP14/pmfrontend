import axios from 'axios';

export default () => {
	axios.defaults.baseURL = "http://localhost:8000/api" 
	axios.defaults.headers.common = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+ localStorage.getItem('token')
	  };
};