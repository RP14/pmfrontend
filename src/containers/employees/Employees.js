import React from "react";
import * as actions from "../../store/actions/index";
import { connect } from "react-redux";

import EmployeesList from "../../components/employees/employeesList/EmployeesList";
import EmployeesSearch from "../../components/employees/employeesList/EmployeesSearch";
import EmployeesPreview from "../../components/employees/employeesPreview/EmployeesPreview";

class Employees extends React.Component {
  componentDidMount() {
    this.props.loadEmployees();
  }

  componentwi;
  render() {
    return (
      <div className="employeesBox">
        <div className="employeesLeftColumn">
          <EmployeesSearch filterEmployees={this.props.filterEmployees} />

          <EmployeesList
            employees={this.props.employees}
            loadEmployee={this.props.loadEmployee}
            loading={this.props.loading}
          />
        </div>
        <div className="employeesRightColumn">
          <EmployeesPreview
            employee={this.props.employee}
            loading={this.props.loading}
          />
        </div>
      </div>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return {
    filterEmployees: searchedTerm =>
      dispatch(actions.filterEmployees(searchedTerm)),
    loadEmployee: email => dispatch(actions.loadEmployee(email)),
    loadEmployees: () => dispatch(actions.loadEmployees())
  };
};

const mapStateToProps = state => {
  return {
    loading: state.userReducer.loading,
    employee: state.employeeReducer.employee,
    employees: state.employeeReducer.employees
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Employees);
