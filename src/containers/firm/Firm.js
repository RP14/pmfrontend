import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../../store/actions/index";
import FirmInfo from "../../components/firm/FirmInfo";
import AddTechnology from "../../components/technology/AddTechnology";
import ProjectTilesList from "../../components/Tiles/ProjectTilesList";
import AddPosition from "../../components/position/AddPosition";
import jwt_decode from "jwt-decode";

import Spinner from "../../components/spinner/Spinner";

class Firm extends Component {
  state = {
    page: 1
  };

  componentDidMount() {
    let token = localStorage.getItem("token");
    let decodedToken = jwt_decode(token);
    this.systemRole = decodedToken.SystemRole;

    this.props.loadProjects(this.state.page);
    //window.addEventListener("scroll", this.loadProjectsLazy);
  }

  /*  componentWillUnmount() {
    window.removeEventListener("scroll", this.loadProjectsLazy);
  } */

  /*  loadProjectsLazy = () => {
    if (window.scrollY >= window.innerHeight * this.state.page) {
      this.setState({ page: this.state.page + 1 }, () => {
        this.props.loadProjects(this.state.page);
      });
    }
  }; */

  render() {
    return (
      <div className="center_of_site">
        <FirmInfo />

        {this.systemRole !== "User" ? (
          <React.Fragment>
            <div className="ownerPanel">
              <AddTechnology
                addTechnology={this.props.addTechnology}
                error={this.props.error}
                errorFetching={this.props.addTechErrorFetching}
              />
            </div>
            <div className="ownerPanel">
              <AddPosition
                addPosition={this.props.addPosition}
                error={this.props.addPositionError}
                errorFetching={this.props.addPositionErrorFetching}
              />
            </div>
          </React.Fragment>
        ) : null}

        {this.props.loading ? (
          <Spinner />
        ) : (
          <ProjectTilesList projects={this.props.projects} />
        )}
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addTechnology: technology => dispatch(actions.addTechnology(technology)),
    addPosition: position => dispatch(actions.addPosition(position)),
    loadProjects: pageNumber => dispatch(actions.loadProjects(null, pageNumber))
  };
};

const mapStateToProps = state => {
  return {
    projects: state.projectReducer.projects,
    loading: state.projectReducer.loading,
    error: state.technologyReducer.error,
    addPositionError: state.positionReducer.error,
    addPositionLoading: state.positionReducer.loading,
    addTechErrorFetching: state.technologyReducer.errorFetching,
    addPositionErrorFetching: state.positionReducer.errorFetching
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Firm);
