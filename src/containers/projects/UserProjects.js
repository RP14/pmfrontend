import React, { Component } from "react";
import * as actions from "../../store/actions/index";
import { connect } from "react-redux";
import AddProject from "../../components/projects/addProject/AddProject";
import ProjectTilesList from "../../components/Tiles/ProjectTilesList";
import Spinner from "../../components/spinner/Spinner";
import jwt_decode from "jwt-decode";

class UserProjects extends Component {
  componentDidMount() {
    let token = localStorage.getItem("token");
    let decodedToken = jwt_decode(token);
    this.systemRole = decodedToken.SystemRole;

    this.props.loadTechnology();
    this.props.loadProjects(decodedToken.email);
    this.props.fetchPositions();
  }

  render() {
    return (
      <div>
        {this.systemRole !== "User" ? (
          <div className="addProjectCenter">
            <AddProject
              addProject={this.props.addProject}
              technologiesOptions={this.props.technologiesOptions}
              positionsOptions={this.props.positionsOptions}
              errorFetching={this.props.addProjectErrorFetching}
              error={this.props.error}
            />{" "}
          </div>
        ) : (
          <React.Fragment>
            {this.props.loading ? (
              <Spinner />
            ) : (
              <React.Fragment>
                <h1 className="userPanelTitle">Twoje projekty</h1>
                <ProjectTilesList projects={this.props.projects} />
              </React.Fragment>
            )}
          </React.Fragment>
        )}
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    loadUser: email => dispatch(actions.loadUser(email)),
    loadTechnology: () => dispatch(actions.loadTechnology()),
    loadProjects: email => dispatch(actions.loadProjects(email)),
    fetchPositions: () => dispatch(actions.fetchPositions()),
    addProject: newProject => dispatch(actions.addProject(newProject))
  };
};

const mapStateToProps = state => {
  return {
    loading: state.projectReducer.loading,
    user: state.userReducer.user,
    projects: state.projectReducer.projects,
    error: state.projectReducer.error,
    technologiesOptions: state.technologyReducer.technologiesOptions,
    positionsOptions: state.positionReducer.positionsOptions,
    addProjectErrorFetching: state.projectReducer.errorFetching
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserProjects);
