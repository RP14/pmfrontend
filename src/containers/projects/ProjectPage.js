import React, { Component } from "react";
import * as actions from "../../store/actions/index";
import { connect } from "react-redux";
import jwt_decode from "jwt-decode";

import ProjectInformation from "../../components/project/projectMain/ProjectInformation";
import TeamLeaderBox from "../../components/project/projectMain/teamLeaderBox/TeamLeaderBox";
import ProjectParticipants from "../../components/project/projectMain/ProjectParticipants";
import ProjectWantedPanel from "../../components/project/projectMain/projectWantedPanel/ProjectWantedPanel";
import Spinner from "../../components/spinner/Spinner";

class ProjectPage extends Component {
  state = {
    showTeamLeaderPanel: false
  };

  componentDidMount() {
    let token = localStorage.getItem("token");
    let decodedToken = jwt_decode(token);

    let projectId = this.props.match.params.projectId;
    this.props.fetchPositions();
    this.props.loadProject(projectId);
    this.props.loadEmployees();
    this.props.loadProjectPositions(projectId);

    let projectTeamLeaderArray = "";
    if (decodedToken.ProjectTeamLeader) {
      if (!Array.isArray(decodedToken.ProjectTeamLeader)) {
        projectTeamLeaderArray = [decodedToken.ProjectTeamLeader];
      } else {
        projectTeamLeaderArray = decodedToken.ProjectTeamLeader;
      }
      const projectTeamLeaderIntArray = projectTeamLeaderArray.map(id =>
        parseInt(id, 10)
      );
      if (projectTeamLeaderIntArray.includes(Number(projectId))) {
        this.setState({ showTeamLeaderPanel: true });
      }
    }
  }

  render() {
    if (this.props.loading) {
      return <Spinner />;
    }
    return (
      <div className="center_of_site">
        {this.state.showTeamLeaderPanel ? (
          <TeamLeaderBox
            projectCandidates={this.props.project.candidates}
            employeesOptions={this.props.employeesOptions}
            positionsOptions={this.props.positionsOptions}
            addPositionsToProject={this.props.addPositionsToProject}
            projectId={this.props.match.params.projectId}
            error={this.props.error}
            errorFetching={this.props.addPositionErrorFetching}
            acceptCandidate={this.props.acceptCandidate}
          />
        ) : null}

        <ProjectInformation project={this.props.project} />
        <div className="project__members">
          <ProjectParticipants
            projectParticipants={this.props.project.participants}
            removeParticipant={this.props.removeParticipant}
            projectId={this.props.match.params.projectId}
            isTeamLeader={this.state.showTeamLeaderPanel}
          />
          <ProjectWantedPanel
            allPositions={this.props.positionsOptions}
            projectPositions={this.props.projectPositions}
            recruitmentEndDate={this.props.project.endDate}
            addCandidate={this.props.addCandidate}
            projectId={this.props.match.params.projectId}
          />
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    loadProject: projectId => dispatch(actions.loadProject(projectId)),
    fetchPositions: () => dispatch(actions.fetchPositions()),
    addPositionsToProject: (projectId, positions) =>
      dispatch(actions.addPositionsToProject(projectId, positions)),
    loadProjectPositions: projectId =>
      dispatch(actions.loadProjectPositions(projectId)),
    addCandidate: (email, projectId, positionId) =>
      dispatch(actions.addCandidate(email, projectId, positionId)),
    loadEmployees: () => dispatch(actions.loadEmployees()),
    acceptCandidate: (projectId, email) =>
      dispatch(actions.acceptCandidate(projectId, email)),
    removeParticipant: (projectId, email) =>
      dispatch(actions.removeParticipant(projectId, email))
  };
};

const mapStateToProps = state => {
  return {
    loading: state.projectReducer.loading,
    error: state.positionReducer.error,
    positionsOptions: state.positionReducer.positionsOptions,
    project: state.projectReducer.project,
    addPositionErrorFetching: state.positionReducer.errorFetching,
    projectPositions: state.positionReducer.projectPositions,
    employeesOptions: state.employeeReducer.employeesOptions
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectPage);
