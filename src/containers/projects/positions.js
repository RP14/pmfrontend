import React, { Component } from "react";
import { fetchPositions } from "../../store/actions/positionActions";
import { connect } from "react-redux";
// to jest kontener co wyswietla pozycje na dole strony do wywalenia
const mapDispatchToProps = dispatch => {
  return {
    fetchPositions: () => dispatch(fetchPositions())
  };
};

const mapStateToProps = state => {
  return {
    positions: state.positionReducer.positions,
    loading: state.positionReducer.loading
  };
};

class Positions extends Component {
  componentDidMount() {
    this.props.fetchPositions();
  }

  render() {
    if (this.props.loading) {
      return <div>Loading... </div>;
    }
    return (
      <div className="mainPage">
        {this.props.positions.map(position => {
          return <p> {position.positionName} </p>;
        })}
      </div>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Positions);
