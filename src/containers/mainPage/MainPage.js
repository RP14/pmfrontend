import React, { Component } from "react";
import { connect } from "react-redux";
import mpPhoto from "../../../src/assets/images/mpphoto2.png";
import { Link } from "react-router-dom";

class Mainpage extends Component {
  render() {
    return (
      <div className="mainPage">
        <div className="mainPageContainer">
          <div className="siteTitle">Team Builder</div>
          <div className="siteInfo">
            Zarządzaj projektami, twórz zespoły i kieruj pracą z zadziwiającą
            łatwością dzięki aplikacji Team Builder.{" "}
          </div>
          <Link to="/firm" className="mainpageButtonLink">
            <button className="mainPageButton" onClick="">
              Zacznij teraz!
            </button>
          </Link>
        </div>
        <div className="mainPagePhoto">
          <img
            className="mainPageImg"
            src={mpPhoto}
            alt="mpPhoto"
            height="650"
            width="450"
          />
        </div>
      </div>
    );
  }
}

export default connect(
  null,
  null
)(Mainpage);
