import React, { Component } from "react";
import * as actions from "../../store/actions/index";
import { connect } from "react-redux";

import NavbarUserProfile from "../../components/navigation/navbarUserProfile.js/NavbarUserProfile";

class UserPanel extends Component {
  render() {
    return (
      <NavbarUserProfile
        loadUser={this.props.loadUser}
        user={this.props.user}
        loading={this.props.loading}
      />
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    loadUser: email => dispatch(actions.loadUser(email))
  };
};

const mapStateToProps = state => {
  return {
    loading: state.userReducer.loading,
    user: state.userReducer.user,
    error: state.userReducer.error
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserPanel);
