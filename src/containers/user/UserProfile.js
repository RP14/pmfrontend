import React, { Component } from "react";
import * as actions from "../../store/actions/index";
import { connect } from "react-redux";
import jwt_decode from "jwt-decode";

import UserInfo from "../../components/userPanel/userInfo/UserInfo";
import UserPhoto from "../../components/userPanel/userPhoto/UserPhoto";
import UserQualiList from "../../components/userPanel/userQualiList/UserQualiList";
import UserQualiAdd from "../../components/userPanel/userQualiAdd/UserQualiAdd";

class UserProfile extends Component {
  componentDidMount() {
    let token = localStorage.getItem("token");
    let decodedToken = jwt_decode(token);
    this.email = decodedToken.email;

    this.props.loadUser(decodedToken.email);
    this.props.loadTechnology(decodedToken.email);
  }

  render() {
    return (
      console.log("db2", this.props),
      (
        <div>
          <div className="userProfileBox">
            <h2 className="userPanelTitle">Twój profil</h2>
            <div className="userPhotoAndInfo">
              <UserPhoto
                user={this.props.user}
                updateUserPhoto={this.props.updateUserPhoto}
              />
              <UserInfo
                user={this.props.user}
                updateUserInfo={this.props.updateUserInfo}
                loading={this.props.userLoading}
              />
            </div>
          </div>
          <div className="qualiBox">
            <UserQualiList
              userEmail={this.email}
              userTechnologies={this.props.userTechnologies}
              removeUserTechnology={this.props.removeUserTechnology}
              loading={this.props.technologyLoading}
            />

            <UserQualiAdd
              userEmail={this.email}
              userOptions={this.props.noUserOptions}
              addUserTechnology={this.props.addUserTechnology}
              fetchNoUserOptions={this.props.fetchNoUserOptions}
            />
          </div>
        </div>
      )
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    loadUser: email => dispatch(actions.loadUser(email)),
    loadTechnology: email => dispatch(actions.loadTechnology(email)),
    removeUserTechnology: (email, id) =>
      dispatch(actions.removeUserTechnology(email, id)),
    addUserTechnology: (email, id) =>
      dispatch(actions.addUserTechnology(email, id)),
    fetchNoUserOptions: () => dispatch(actions.fetchNoUserOptions()),
    updateUserInfo: (email, newUserInfo, element) =>
      dispatch(actions.updateUserInfo(email, newUserInfo, element)),
    updateUserPhoto: (email, dataFormFile) =>
      dispatch(actions.updateUserPhoto(email, dataFormFile))
  };
};

const mapStateToProps = state => {
  return {
    userLoading: state.userReducer.loading,
    technologyLoading: state.technologyReducer.loading,
    user: state.userReducer.user,
    userTechnologies: state.technologyReducer.userTechnologies,
    technologiesOptions: state.technologyReducer.technologiesOptions,
    userTechnologiesOptions: state.technologyReducer.userTechnologiesOptions,
    noUserOptions: state.technologyReducer.noUserOptions
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserProfile);
