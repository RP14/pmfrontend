import React, { Component } from "react";
import * as actions from "../../../store/actions/index";
import { connect } from "react-redux";
import TeamBuilderLogo from "../../../assets/images/logo.png";
import { Link } from 'react-router-dom';

class Confirmation extends Component {
  state = {
    error: ""
  };

  componentDidMount() {
    let emailAddress = this.props.match.params.emailAddress;
    let userCode = this.props.match.params.userCode;
    this.props.confirmAccount(emailAddress, userCode);
  }

  render() {
    return (
      <div className="bg">
        <div className="bgImg">
          <div className="flex">
            <div className="whiteBox">
              <div>
                  <div>
                    <img
                      src={TeamBuilderLogo}
                      className="loginLogo"
                      alt="TeamBuilderLogo"
                    />
                    <div className="siteName">Team Builder</div>
                  </div>

                  {this.props.response ? <div><div className="loginTitle">Twoje konto zostało potwierdzone.</div> Przejdź na <Link to="/auth">stronę logowania</Link>, aby się zalogować.</div> : null }
                  {this.props.error ? <div><div className="loginTitle">Wystąpił błąd.</div><Link to="/auth">Powrót</Link></div> : null}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    confirmAccount: ( email, code ) => dispatch(actions.confirmAccount(email, code))
  };
};

const mapStateToProps = state => {
  return {
    loading: state.authReducer.loading,
    error: state.authReducer.error,
    response: state.authReducer.response
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Confirmation);
