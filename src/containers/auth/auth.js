import React, { Component } from "react";
import * as actions from "../../store/actions/index";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import Register from "../../components/auth/register/Register";
import Login from "../../components/auth/login/Login";
import TeamBuilderLogo from "../../assets/images/logo.png";

class Auth extends Component {
  state = {
    isRegister: false
  };

  switchAuthModeHandler = () => {
    this.setState(prevState => {
      return { isRegister: !prevState.isRegister };
    });
    this.props.resetErrorMessage();
  };

  render() {
    let authRedirect = null;

    if (this.props.isAuthenticated) {
      authRedirect = <Redirect to="/" />;
    }

    return (
      <div className="bg">
        {authRedirect}
        <div className="bgImg">
          <div className="flex">
            <div className="whiteBox">
              {/* <div className="loginLogo"></div> */}
              {this.state.isRegister ? (
                <div>
                  <div className="siteName">Team Builder</div>
                  <div className="loginTitle">Załóż konto</div>
                </div>
              ) : (
                <div>
                  <img
                    src={TeamBuilderLogo}
                    className="loginLogo"
                    alt="TeamBuilderLogo"
                  />
                  <div className="siteName">Team Builder</div>
                  <div className="loginTitle">Zaloguj się</div>
                </div>
              )}
              {this.state.isRegister ? (
                <Register
                  submitRegister={this.props.register}
                  response={this.props.response}
                />
              ) : (
                <Login submitLogin={this.props.login} />
              )}

              {this.props.error}

              <div className="noAccountFooter">
                <div className="noAccountDesc">
                  {this.state.isRegister
                    ? "Masz już konto?"
                    : "Przejdz do rejestracji"}{" "}
                </div>
                <div
                  className="goToRegisterLink"
                  onClick={this.switchAuthModeHandler}
                >
                  Przejdź{" "}
                  {this.state.isRegister
                    ? "do logowania"
                    : "na stronę rejestracji"}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    register: user => dispatch(actions.register(user)),
    login: user => dispatch(actions.login(user)),
    resetErrorMessage: () => dispatch(actions.resetErrorMessage())
  };
};

const mapStateToProps = state => {
  return {
    isAuthenticated: state.authReducer.token !== null,
    loading: state.authReducer.loading,
    error: state.authReducer.error,
    response: state.authReducer.response
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Auth);
