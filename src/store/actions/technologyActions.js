import * as types from "../actions/actionTypes";
import axios from "axios";

export const loadTechnologyStart = () => {
  return {
    type: types.LOAD_TECHNOLOGIES_START
  };
};

export const loadTechnologySuccess = technologies => {
  return {
    technologies: technologies,
    type: types.LOAD_TECHNOLOGIES_SUCCESS,
    error: null
  };
};

export const loadTechnologyFailed = error => {
  return {
    type: types.LOAD_TECHNOLOGIES_FAILED,
    error: error
  };
};

export const loadTechnology = email => {
  return dispatch => {
    dispatch(loadTechnologyStart());
    axios
      .get("/Technology")
      .then(response => {
        dispatch(loadTechnologySuccess(response.data));
        if (email) {
          dispatch(loadUserTechnology(email));
        }
      })
      .catch(error => {
        console.log(error);
        dispatch(loadTechnologyFailed(error.response));
      });
  };
};

export const addTechnologyStart = () => {
  return {
    type: types.ADD_TECHNOLOGIES_START
  };
};

export const addTechnologySuccess = response => {
  return {
    response: response,
    type: types.ADD_TECHNOLOGIES_SUCCESS
  };
};

export const addTechnologyFailed = error => {
  return {
    type: types.ADD_TECHNOLOGIES_FAILED,
    error: error
  };
};

export const addTechnology = technology => {
  return dispatch => {
    dispatch(addTechnologyStart());

    let data = {
      technologyName: technology
    };

    axios
      .post("/Technology", data)
      .then(response => {
        dispatch(addTechnologySuccess(response));
      })
      .catch(error => {
        dispatch(addTechnologyFailed(error.response));
      });
  };
};

//======= USER TECHNOLOGIES =======//

export const loadUserTechnologyStart = () => {
  return {
    type: types.LOAD_USER_TECHNOLOGIES_START
  };
};

export const loadUserTechnologySuccess = userTechnologies => {
  return {
    userTechnologies: userTechnologies,
    type: types.LOAD_USER_TECHNOLOGIES_SUCCESS,
    error: null
  };
};

export const loadUserTechnologyFailed = error => {
  return {
    type: types.LOAD_USER_TECHNOLOGIES_FAILED,
    error: error
  };
};

export const loadUserTechnology = email => {
  return dispatch => {
    dispatch(loadUserTechnologyStart());

    axios
      .get("/User/" + email + "/technologies")
      .then(response => {
        dispatch(loadUserTechnologySuccess(response.data));
      })
      .catch(error => {
        console.log(error);
        dispatch(loadUserTechnologyFailed(error.response));
      });
  };
};

export const addUserTechnologyStart = () => {
  return {
    type: types.ADD_TECHNOLOGIES_START
  };
};

export const addUserTechnologySuccess = response => {
  return {
    response: response,
    type: types.ADD_TECHNOLOGIES_SUCCESS,
    error: null
  };
};

export const addUserTechnologyFailed = error => {
  return {
    type: types.ADD_TECHNOLOGIES_FAILED,
    error: error
  };
};

export const addUserTechnology = (email, technologyId) => {
  return dispatch => {
    dispatch(addUserTechnologyStart());

    let data = {
      userEmailAddress: email,
      technologyId: technologyId
    };

    axios
      .post("/User/technology", data)
      .then(response => {
        dispatch(addUserTechnologySuccess(response));
        dispatch(loadUserTechnology(email));
      })
      .catch(error => {
        console.log(error);
        dispatch(addUserTechnologyFailed(error.response));
      });
  };
};

export const removeUserTechnologyStart = () => {
  return {
    type: types.REMOVE_USER_TECHNOLOGIES_START
  };
};

export const removeUserTechnologySuccess = (response, technologyId) => {
  return {
    response: response,
    technologyId: technologyId,
    type: types.REMOVE_USER_TECHNOLOGIES_SUCCESS,
    error: null
  };
};

export const removeUserTechnologyFailed = error => {
  return {
    type: types.REMOVE_USER_TECHNOLOGIES_FAILED,
    error: error
  };
};

export const removeUserTechnology = (email, technologyId) => {
  return dispatch => {
    dispatch(removeUserTechnologyStart());
    axios
      .delete("/User/" + email + "/technology/" + technologyId)
      .then(response => {
        dispatch(removeUserTechnologySuccess(response, technologyId));
        dispatch(loadUserTechnology(email));
      })
      .catch(error => {
        console.log(error);
        dispatch(removeUserTechnology(error.response));
      });
  };
};
