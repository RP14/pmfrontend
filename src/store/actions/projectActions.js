import * as actionTypes from "./actionTypes";
import axios from "axios";
import { getNewToken } from "../actions/userActions";

export const addProjectStart = () => {
  return {
    type: actionTypes.ADD_PROJECT_START
  };
};

export const addProjectSuccess = response => {
  return {
    type: actionTypes.ADD_PROJECT_SUCCESS,
    response: response
  };
};

export const addProjectFailed = error => {
  return {
    type: actionTypes.ADD_PROJECT_FAILED,
    error: error
  };
};

export const addProject = (newProject, history) => {
  return dispatch => {
    dispatch(addProjectStart());
    const newProjectData = {
      name: newProject.name,
      description: newProject.description,
      startDate: newProject.startDate,
      endDate: newProject.endDate,
      status: newProject.status,
      teamLeaderEmailAddress: newProject.teamLeaderEmail,
      teamLeaderPositionId: newProject.teamLeaderPositionId,
      technologies: newProject.technologies
    };
    axios
      .post(`/Project`, newProjectData)
      .then(response => {
        dispatch(addProjectSuccess(response));
        dispatch(getNewToken(localStorage.email));
        history.push(`/project/${response.data.projectId}`);
      })
      .catch(error => {
        console.log(error);
        dispatch(addProjectFailed(error.response));
      });
  };
};

export const loadProjectsStart = () => {
  return {
    type: actionTypes.LOAD_PROJECTS_START
  };
};

export const loadProjectsSuccess = projects => {
  return {
    type: actionTypes.LOAD_PROJECTS_SUCCESS,
    projects: projects
  };
};

export const loadProjectsFailed = error => {
  return {
    type: actionTypes.LOAD_PROJECTS_FAILED,
    error: error
  };
};

export const loadProjects = (email, pageNumber = 1) => {
  const body = {
    params: {
      pageSize: 9,
      pageNumber: pageNumber
    }
  };

  return dispatch => {
    let url = "/Project/paginated";

    if (email) {
      url = "/User/" + email + "/projects";
    }

    dispatch(loadProjectsStart());
    return axios
      .get(url, body)
      .then(response => {
        dispatch(loadProjectsSuccess(response.data));
      })
      .catch(error => {
        dispatch(loadProjectsFailed(error.message));
      });
  };
};

export const loadProjectStart = () => {
  return {
    type: actionTypes.LOAD_PROJECT_START
  };
};

export const loadProjectSuccess = project => {
  return {
    type: actionTypes.LOAD_PROJECT_SUCCESS,
    project: project
  };
};

export const loadProjectFailed = error => {
  return {
    type: actionTypes.LOAD_PROJECT_FAILED,
    error: error
  };
};

export const loadProject = projectId => {
  return dispatch => {
    dispatch(loadProjectStart());
    return axios
      .get("/Project/" + projectId)
      .then(response => {
        dispatch(loadProjectSuccess(response.data));
      })
      .catch(error => {
        dispatch(loadProjectFailed(error.message));
      });
  };
};
