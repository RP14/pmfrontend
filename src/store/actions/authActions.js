import * as types from "../actions/actionTypes";
import axios from "axios";

export const resetErrorMessage = () => {
  return {
    type: types.RESET_ERROR_MESSAGE,
    error: null
  };
};

export const registerStart = () => {
  return {
    type: types.REGISTER_START
  };
};

export const registerSuccess = response => {
  return {
    type: types.REGISTER_SUCCESS,
    email: null,
    response: response
  };
};

export const registerFailed = error => {
  return {
    type: types.REGISTER_FAILED,
    error: error
  };
};

export const register = user => {
  return dispatch => {
    dispatch(registerStart());

    const authData = {
      emailAddress: user.email,
      firstname: user.name,
      lastname: user.surname,
      password: user.password,
      confirmPassword: user.confirmPassword
    };

    axios
      .post("/User/register", JSON.stringify(authData), {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then(response => {
        dispatch(registerSuccess(response.status));
      })
      .catch(error => {
        console.log(error);
        dispatch(registerFailed(error.response.data));
      });
  };
};

export const loginStart = () => {
  return {
    type: types.LOGIN_START
  };
};

export const loginSuccess = token => {
  return {
    type: types.LOGIN_SUCCESS,
    token: token
  };
};

export const loginFailed = error => {
  return {
    type: types.LOGIN_FAILED,
    error: error
  };
};

export const logout = () => {
  localStorage.removeItem("token");
  return {
    type: types.LOGOUT
  };
};

export const login = user => {
  return dispatch => {
    dispatch(loginStart());

    const authData = {
      emailAddress: user.email,
      password: user.password
    };

    axios
      .post("/User/login", JSON.stringify(authData), {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then(response => {
        localStorage.setItem("token", response.data);
        localStorage.setItem("email", user.email);
        dispatch(loginSuccess(response.data));
      })
      .catch(error => {
        dispatch(loginFailed(error.response.data));
      });
  };
};

export const confirmAccountStart = () => {
  return {
    type: types.CONFIRM_ACCOUNT_START
  };
};

export const confirmAccountSuccess = response => {
  return {
    type: types.CONFIRM_ACCOUNT_SUCCESS,
    response: response
  };
};

export const confirmAccountFailed = error => {
  return {
    type: types.CONFIRM_ACCOUNT_FAILED,
    error: error
  };
};

export const confirmAccount = (email, code) => {
  return dispatch => {
    dispatch(confirmAccountStart());

    axios
      .post("/User/confirm/" + email + "/" + code, {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
      .then(response => {
        dispatch(confirmAccountSuccess(response.data));
      })
      .catch(error => {
        dispatch(confirmAccountFailed(error.response));
      });
  };
};
