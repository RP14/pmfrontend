import * as types from "../actions/actionTypes";
import axios from "axios";
import { loadProject } from "../actions/projectActions";
import { loadProjectPositions } from "../actions/positionActions";

export const loadEmployeeStart = () => {
  return {
    type: types.LOAD_EMPLOYEE_START
  };
};

export const loadEmployeeSuccess = employee => {
  return {
    employee: employee,
    type: types.LOAD_EMPLOYEE_SUCCESS,
    error: null
  };
};

export const loadEmployeeFailed = error => {
  return {
    type: types.LOAD_EMPLOYEE_FAILED,
    error: error
  };
};

export const loadEmployee = email => {
  return dispatch => {
    dispatch(loadEmployeeStart());

    axios
      .get("/User/" + email)
      .then(response => {
        dispatch(loadEmployeeSuccess(response.data));
      })
      .catch(error => {
        console.log(error);
        dispatch(loadEmployeeFailed(error.response));
      });
  };
};

export const loadEmployeesStart = () => {
  return {
    type: types.LOAD_EMPLOYEES_START
  };
};

export const loadEmployeesSuccess = users => {
  return {
    users: users,
    type: types.LOAD_EMPLOYEES_SUCCESS,
    error: null
  };
};

export const loadEmployeesFailed = error => {
  return {
    type: types.LOAD_EMPLOYEES_FAILED,
    error: error
  };
};

export const loadEmployees = () => {
  return dispatch => {
    dispatch(loadEmployeesStart());

    axios
      .get("/User")
      .then(response => {
        dispatch(loadEmployeesSuccess(response.data));
      })
      .catch(error => {
        console.log(error);
        dispatch(loadEmployeesFailed(error.response));
      });
  };
};

export const filterEmployeesStart = () => {
  return {
    type: types.FILTER_EMPLOYEES_START
  };
};

export const filterEmployeesSuccess = users => {
  return {
    users: users,
    type: types.FILTER_EMPLOYEES_SUCCESS,
    error: null
  };
};

export const filterEmployeesFailed = error => {
  return {
    type: types.FILTER_EMPLOYEES_FAILED,
    error: error
  };
};

export const filterEmployees = searchingTerm => {
  return dispatch => {
    dispatch(filterEmployeesStart());

    axios
      .get("/User/paginated", {
        params: {
          SearchQuery: searchingTerm
        }
      })
      .then(response => {
        dispatch(filterEmployeesSuccess(response.data));
      })
      .catch(error => {
        console.log(error);
        dispatch(filterEmployeesFailed(error.response));
      });
  };
};

export const addCandidateStart = () => {
  return {
    type: types.ADD_CANDIDATE_START
  };
};

export const addCandidateSuccess = response => {
  return {
    type: types.ADD_CANDIDATE_SUCCESS,
    response: response
  };
};

export const addCandidateFailed = error => {
  return {
    type: types.ADD_CANDIDATE_FAILED,
    error: error
  };
};

export const addCandidate = (email, projectId, positionId) => {
  return dispatch => {
    dispatch(addCandidateStart());

    const data = {
      userEmailAddress: email,
      projectId: projectId,
      positionId: positionId
    };
    axios
      .post("/Project/candidate", data)
      .then(response => {
        dispatch(addCandidateSuccess(response));
        dispatch(loadProject(projectId));
        dispatch(loadProjectPositions(projectId));
      })
      .catch(error => {
        console.log(error);
        dispatch(addCandidateFailed(error.response));
      });
  };
};

export const acceptCandidateStart = () => {
  return {
    type: types.ACCEPT_CANDIDATE_START
  };
};

export const acceptCandidateSuccess = response => {
  return {
    type: types.ACCEPT_CANDIDATE_SUCCESS,
    response: response
  };
};

export const acceptCandidateFailed = error => {
  return {
    type: types.ACCEPT_CANDIDATE_FAILED,
    error: error
  };
};

export const acceptCandidate = (projectId, email) => {
  return dispatch => {
    dispatch(acceptCandidateStart());

    const data = {
      projectId: projectId,
      userEmailAddress: email
    };
    axios
      .post("/Project/participant", data)
      .then(response => {
        dispatch(acceptCandidateSuccess(response));
        dispatch(loadProject(projectId));
      })
      .catch(error => {
        console.log(error);
        dispatch(acceptCandidateFailed(error.response));
      });
  };
};

export const removeParticipantStart = () => {
  return {
    type: types.REMOVE_PARTICIPANT_START
  };
};

export const removeParticipantSuccess = response => {
  return {
    type: types.REMOVE_PARTICIPANT_SUCCESS,
    response: response
  };
};

export const removeParticipantFailed = error => {
  return {
    type: types.REMOVE_PARTICIPANT_FAILED,
    error: error
  };
};

export const removeParticipant = (projectId, email) => {
  return dispatch => {
    dispatch(removeParticipantStart());

    axios
      .delete("/Project/" + projectId + "/participant/" + email)
      .then(response => {
        dispatch(removeParticipantSuccess(response));
        dispatch(loadProject(projectId));
      })
      .catch(error => {
        console.log(error);
        dispatch(removeParticipant(error.response));
      });
  };
};
