import * as actionTypes from "../actions/actionTypes";
import axios from "axios";

export const fetchPositionsStart = () => {
  return {
    type: actionTypes.FETCH_POSITIONS_START
  };
};

export const fetchPositionsSuccess = positions => {
  return {
    type: actionTypes.FETCH_POSITIONS_SUCCESS,
    positions: positions
  };
};

export const fetchPositionsFailed = error => {
  return {
    type: actionTypes.FETCH_POSITIONS_FAILED,
    error: error
  };
};

export const fetchPositions = () => {
  return dispatch => {
    dispatch(fetchPositionsStart());
    return axios
      .get("/Position")
      .then(response => {
        dispatch(fetchPositionsSuccess(response.data));
      })
      .catch(error => {
        console.log(error);
        dispatch(fetchPositionsFailed(error.message));
      });
  };
};

export const addPositionStart = () => {
  return {
    type: actionTypes.ADD_POSITIONS_START
  };
};

export const addPositionSuccess = response => {
  return {
    response: response,
    type: actionTypes.ADD_POSITIONS_SUCCESS,
    error: null
  };
};

export const addPositionFailed = error => {
  return {
    error: error,
    type: actionTypes.ADD_POSITIONS_FAILED
  };
};

export const addPosition = position => {
  return dispatch => {
    dispatch(addPositionStart());

    let data = {
      positionName: position
    };

    axios
      .post("/position", data)
      .then(response => {
        dispatch(addPositionSuccess(response.data));
      })
      .catch(error => {
        dispatch(addPositionFailed(error.response));
        console.log(error);
      });
  };
};

export const addPositionsToProjectStart = () => {
  return {
    type: actionTypes.ADD_POSITIONS_TO_PROJECT_START
  };
};

export const addPositionsToProjectSuccess = projectPositions => {
  return {
    projectPosition: projectPositions,
    type: actionTypes.ADD_POSITIONS_TO_PROJECT_SUCCESS,
    error: null
  };
};

export const addPositionsToProjectFailed = error => {
  return {
    type: actionTypes.ADD_POSITIONS_TO_PROJECT_FAILED,
    error: error
  };
};

export const addPositionsToProject = (projectId, positions) => {
  return dispatch => {
    dispatch(addPositionsToProjectStart());

    let data = {
      projectId: projectId,
      positions: positions
    };

    axios
      .post("/Project/positions", data)
      .then(response => {
        dispatch(addPositionsToProjectSuccess(response));
        dispatch(fetchPositions());
        dispatch(loadProjectPositions(projectId));
      })
      .catch(error => {
        dispatch(addPositionsToProjectFailed(error));
      });
  };
};

export const loadProjectPositionsStart = () => {
  return {
    type: actionTypes.LOAD_PROJECT_POSITIONS_START
  };
};

export const loadProjectPositionsSuccess = projectPositions => {
  return {
    type: actionTypes.LOAD_PROJECT_POSITIONS_SUCCESS,
    projectPositions: projectPositions
  };
};

export const loadProjectPositionsFailed = error => {
  return {
    type: actionTypes.LOAD_PROJECT_POSITIONS_FAILED,
    error: error
  };
};

export const loadProjectPositions = projectId => {
  return dispatch => {
    dispatch(loadProjectPositionsStart());
    return axios
      .get("/Project/" + projectId + "/entries")
      .then(response => {
        dispatch(loadProjectPositionsSuccess(response.data));
      })
      .catch(error => {
        console.log(error);
        dispatch(loadProjectPositionsFailed(error.response));
      });
  };
};
