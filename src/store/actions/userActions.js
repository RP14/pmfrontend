import * as types from "../actions/actionTypes";
import axios from "axios";

export const loadUserStart = () => {
  return {
    type: types.LOAD_USER_START
  };
};

export const loadUserSuccess = user => {
  return {
    user: user,
    type: types.LOAD_USER_SUCCESS,
    error: null
  };
};

export const loadUserFailed = error => {
  return {
    type: types.LOAD_USER_FAILED,
    error: error
  };
};

export const loadUser = email => {
  return dispatch => {
    dispatch(loadUserStart());

    axios
      .get("/User/" + email)
      .then(response => {
        dispatch(loadUserSuccess(response.data));
      })
      .catch(error => {
        console.log(error);
        dispatch(loadUserFailed(error.response));
      });
  };
};

export const updateUserInfoStart = () => {
  return {
    type: types.UPDATE_USER_INFO_START
  };
};

export const updateUserInfoSuccess = response => {
  return {
    response: response,
    type: types.UPDATE_USER_INFO_SUCCESS,
    error: null
  };
};

export const updateUserInfoFailed = error => {
  return {
    type: types.UPDATE_USER_INFO_FAILED,
    error: error
  };
};

export const updateUserInfo = (email, newUserInfo, element) => {
  return dispatch => {
    dispatch(updateUserInfoStart());

    let data = [
      {
        value: newUserInfo,
        path: element,
        op: "replace"
      }
    ];

    axios
      .patch("/User/" + email + "/userUpdate", data)
      .then(response => {
        dispatch(updateUserInfoSuccess(response.data));
        dispatch(getNewToken(email));
        dispatch(loadUser(email));
      })
      .catch(error => {
        console.log("niezaktualizowało user info");
        console.log(error);
        dispatch(updateUserInfoFailed(error.response));
      });
  };
};

export const updateUserPhotoStart = () => {
  return {
    type: types.UPDATE_USER_INFO_START
  };
};

export const updateUserPhotoSuccess = response => {
  return {
    response: response,
    type: types.UPDATE_USER_INFO_SUCCESS
  };
};

export const updateUserPhotoFailed = error => {
  return {
    error: error,
    type: types.UPDATE_USER_INFO_FAILED
  };
};

export const updateUserPhoto = (email, formDataFile) => {
  return dispatch => {
    dispatch(updateUserPhotoStart());

    axios
      .post("/photo/" + email, formDataFile)
      .then(response => {
        dispatch(updateUserPhotoSuccess(response.data));
        dispatch(getNewToken(email));
        dispatch(loadUser(email));
      })
      .catch(error => {
        console.log("nie przeszlo prze axios", error);
        dispatch(updateUserPhotoFailed(error.response));
      });
  };
};

export const getNewTokenStart = () => {
  return {
    type: types.GET_NEW_TOKEN_START
  };
};

export const getNewTokenSuccess = response => {
  return {
    type: types.GET_NEW_TOKEN_SUCCESS,
    response: response
  };
};

export const getNewTokenFailed = error => {
  return {
    type: types.GET_NEW_TOKEN_FAILED,
    error: error
  };
};

export const getNewToken = email => {
  return dispatch => {
    dispatch(getNewTokenStart());

    axios
      .get("/User/" + email + "/GetNewToken")
      .then(response => {
        dispatch(getNewTokenSuccess(response.data));
      })
      .catch(error => {
        dispatch(getNewTokenFailed(error.response));
      });
  };
};
