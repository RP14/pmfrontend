import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../utility";

const initialState = {
  technologies: [],
  technologiesOptions: [],
  userTechnologies: [],
  userTechnologiesOptions: [],
  noUserOptions: [],
  error: null,
  loading: false,
  errorFetching: ""
};

const loadTechnologyStart = state => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const loadTechnologySuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: false,
    technologies: action.technologies,
    technologiesOptions: action.technologies.map(option => {
      return { value: option.id, label: option.technologyName };
    })
  });
};

const loadTechnologyFailed = (state, action) => {
  return updateObject(state, {
    email: null,
    error: action.error,
    loading: false
  });
};

const addTechnologyStart = state => {
  return updateObject(state, {
    error: null,
    loading: true,
    errorFetching: ""
  });
};

const addTechnologySuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: false,
    technologies: action.technologies,
    errorFetching: "fetched"
  });
};

const addTechnologyFailed = (state, action) => {
  return updateObject(state, {
    email: "",
    error: action.error,
    loading: false,
    errorFetching: "fetched"
  });
};

//======= USER TECHNOLOGIES =======//

const loadUserTechnologyStart = state => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const loadUserTechnologySuccess = (state, action) => {
  let noUserOptions = [];
  const avaiableTechnologiesOptions = [...state.technologiesOptions];

  if (action.userTechnologies.length === 0) {
    noUserOptions = state.technologiesOptions;
  } else {
    let userTechnologiesOptions = action.userTechnologies.map(option => {
      return { value: option.id, label: option.technologyName };
    });

    for (var i = 0; i < avaiableTechnologiesOptions.length; i++) {
      for (var j = 0; j < userTechnologiesOptions.length; j++) {
        if (
          JSON.stringify(avaiableTechnologiesOptions[i]) ===
          JSON.stringify(userTechnologiesOptions[j])
        ) {
          avaiableTechnologiesOptions.splice(i, 1);
        }
        noUserOptions = avaiableTechnologiesOptions;
      }
    }
  }

  const updatedState = {
    userTechnologies: action.userTechnologies,
    noUserOptions: noUserOptions,
    error: null,
    loading: false
  };
  return updateObject(state, updatedState);
};

const loadUserTechnologyFailed = (state, action) => {
  return updateObject(state, {
    email: null,
    error: action.error,
    redirect: action.redirect,
    loading: false
  });
};

const addUserTechnologyStart = state => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const addUserTechnologySuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: false,
    userTechnologies: action.userTechnologies
  });
};

const addUserTechnologyFailed = (state, action) => {
  return updateObject(state, {
    email: null,
    error: action.error,
    loading: false
  });
};

const removeUserTechnologyStart = state => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const removeUserTechnologySuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: false,
    userTechnologies: action.userTechnologies
  });
};

const removeUserTechnologyFailed = (state, action) => {
  return updateObject(state, {
    email: null,
    error: action.error,
    loading: false
  });
};

const technologyReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOAD_TECHNOLOGIES_START:
      return loadTechnologyStart(state);
    case actionTypes.LOAD_TECHNOLOGIES_SUCCESS:
      return loadTechnologySuccess(state, action);
    case actionTypes.LOAD_TECHNOLOGIES_FAILED:
      return loadTechnologyFailed(state, action);

    case actionTypes.ADD_TECHNOLOGIES_START:
      return addTechnologyStart(state);
    case actionTypes.ADD_TECHNOLOGIES_SUCCESS:
      return addTechnologySuccess(state, action);
    case actionTypes.ADD_TECHNOLOGIES_FAILED:
      return addTechnologyFailed(state, action);

    case actionTypes.LOAD_USER_TECHNOLOGIES_START:
      return loadUserTechnologyStart(state);
    case actionTypes.LOAD_USER_TECHNOLOGIES_SUCCESS:
      return loadUserTechnologySuccess(state, action);
    case actionTypes.LOAD_USER_TECHNOLOGIES_FAILED:
      return loadUserTechnologyFailed(state, action);

    case actionTypes.ADD_USER_TECHNOLOGIES_START:
      return addUserTechnologyStart(state);
    case actionTypes.ADD_USER_TECHNOLOGIES_SUCCESS:
      return addUserTechnologySuccess(state, action);
    case actionTypes.ADD_USER_TECHNOLOGIES_FAILED:
      return addUserTechnologyFailed(state, action);

    case actionTypes.REMOVE_USER_TECHNOLOGIES_START:
      return removeUserTechnologyStart(state);
    case actionTypes.REMOVE_USER_TECHNOLOGIES_SUCCESS:
      return removeUserTechnologySuccess(state, action);
    case actionTypes.REMOVE_USER_TECHNOLOGIES_FAILED:
      return removeUserTechnologyFailed(state, action);

    default:
      return state;
  }
};

export default technologyReducer;
