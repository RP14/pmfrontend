import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../utility";

const initialState = {
  user: {},
  error: null,
  loading: false
};

const loadUserStart = state => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const loadUserSuccess = (state, action) => {
  return updateObject(state, {
    user: action.user,
    error: null,
    loading: false
  });
};

const loadUserFailed = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};

const updateUserInfoStart = state => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const updateUserInfoSuccess = state => {
  return updateObject(state, {
    loading: false
  });
};

const updateUserInfoFailed = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};

const updateUserPhotoStart = state => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const updateUserPhotoSuccess = state => {
  return updateObject(state, {
    loading: false
  });
};

const updateUserPhotoFailed = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};

const getNewTokenStart = state => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const getNewTokenSuccess = state => {
  return updateObject(state, {
    loading: false
  });
};

const getNewTokenFailed = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOAD_USER_START:
      return loadUserStart(state);
    case actionTypes.LOAD_USER_SUCCESS:
      return loadUserSuccess(state, action);
    case actionTypes.LOAD_USER_FAILED:
      return loadUserFailed(state, action);

    case actionTypes.UPDATE_USER_INFO_START:
      return updateUserInfoStart(state);
    case actionTypes.UPDATE_USER_INFO_SUCCESS:
      return updateUserInfoSuccess(state);
    case actionTypes.UPDATE_USER_INFO_FAILED:
      return updateUserInfoFailed(state, action);

    case actionTypes.UPDATE_USER_PHOTO_START:
      return updateUserPhotoStart(state);
    case actionTypes.UPDATE_USER_PHOTO_SUCCESS:
      return updateUserPhotoSuccess(state);
    case actionTypes.UPDATE_USER_PHOTO_FAILED:
      return updateUserPhotoFailed(state, action);

    case actionTypes.GET_NEW_TOKEN_START:
      return getNewTokenStart(state);
    case actionTypes.GET_NEW_TOKEN_SUCCESS:
      return getNewTokenSuccess(state);
    case actionTypes.GET_NEW_TOKEN_FAILED:
      return getNewTokenFailed(state, action);

    default:
      return state;
  }
};

export default userReducer;
