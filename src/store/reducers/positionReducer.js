import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../utility";

const initialState = {
  positions: [],
  positionsOptions: [],
  projectPositions: [],
  error: null,
  loading: false,
  errorFetching: ""
};

//============= POSITIONS ==================//

const fetchPositionsStart = state => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const fetchPositionsSuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: false,
    positions: action.positions,
    positionsOptions: action.positions.map(option => {
      return { value: option.id, label: option.positionName };
    })
  });
};

const fetchPositionFailed = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};

//============== ADD POSITION TO DATABASE =============//

const addPositionStart = state => {
  return updateObject(state, {
    error: null,
    loading: true,
    errorFetching: ""
  });
};

const addPositionSuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: false,
    position: action.position,
    errorFetching: "fetched"
  });
};

const addPositionFailed = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
    position: action.position,
    errorFetching: "fetched"
  });
};

//===============ADD POSITION TO PROJECT==============//

const addPositionsToProjectStart = state => {
  return updateObject(state, {
    error: null,
    loading: true,
    errorFetching: ""
  });
};

const addPositionsToProjectSuccess = (state, action) => {
  return updateObject(state, {
    projectPositions: action.projectPositions,
    error: null,
    loading: false,
    errorFetching: "fetched"
  });
};

const addPositionsToProjectFailed = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
    errorFetching: "fetched"
  });
};

const loadProjectPositionsStart = state => {
  return updateObject(state, {
    loading: true,
    error: null
  });
};

const loadProjectPositionsSuccess = (state, action) => {
  return updateObject(state, {
    loading: false,
    error: null,
    projectPositions: action.projectPositions
  });
};

const loadProjectPositionsFailed = (state, action) => {
  return updateObject(state, {
    loading: false,
    error: action.error
  });
};

const positionReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_POSITIONS_START:
      return fetchPositionsStart(state);
    case actionTypes.FETCH_POSITIONS_SUCCESS:
      return fetchPositionsSuccess(state, action);
    case actionTypes.FETCH_POSITIONS_FAILED:
      return fetchPositionFailed(state, action);

    case actionTypes.ADD_POSITIONS_START:
      return addPositionStart(state);
    case actionTypes.ADD_POSITIONS_SUCCESS:
      return addPositionSuccess(state, action);
    case actionTypes.ADD_POSITIONS_FAILED:
      return addPositionFailed(state, action);

    case actionTypes.ADD_POSITIONS_TO_PROJECT_START:
      return addPositionsToProjectStart(state);
    case actionTypes.ADD_POSITIONS_TO_PROJECT_SUCCESS:
      return addPositionsToProjectSuccess(state, action);
    case actionTypes.ADD_POSITIONS_TO_PROJECT_FAILED:
      return addPositionsToProjectFailed(state, action);

    case actionTypes.LOAD_PROJECT_POSITIONS_START:
      return loadProjectPositionsStart(state);
    case actionTypes.LOAD_PROJECT_POSITIONS_SUCCESS:
      return loadProjectPositionsSuccess(state, action);
    case actionTypes.LOAD_PROJECT_POSITIONS_FAILED:
      return loadProjectPositionsFailed(state, action);

    default:
      return state;
  }
};

export default positionReducer;
