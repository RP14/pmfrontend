
import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    email: null,
    token: null,
    error: null,
    loading: false
}

const registerStart = ( state ) => {
    return updateObject( state, { 
        error: null, 
        loading: true});
}

const registerSuccess = ( state, action ) => {
    return updateObject( state, {
        email: null,
        error: null, loading: false,
        response: action.response});
}

const registerFailed = ( state, action ) => {
    return updateObject( state, {
        email: null,
        error: action.error,
        loading: false});
}

const loginStart = ( state ) => {
    return updateObject( state, { 
        error: null, 
        loading: true});
}

const loginSuccess = ( state, action ) => {
    return updateObject( state, {
        token: action.token,
        error: null, loading: false});
}

const loginFailed = ( state, action ) => {
    return updateObject( state, {
        error: action.error,
        redirect: action.redirect,
        loading: false });
}

const logout = ( state ) => {
    return updateObject( state, {
        token: null});
}

const confirmAccountStart = ( state ) => {
    return updateObject( state, { 
        error: null, 
        loading: true});
}

const confirmAccountSuccess = ( state, action ) => {
    return updateObject( state, {
        response: action.response,
        error: null, loading: false});
}

const confirmAccountFailed = ( state, action ) => {
    return updateObject( state, {
        error: action.error,
        loading: false });
}


const resetErrorMessage = ( state, action ) => {
    return updateObject(state, {
      error: action.error
    });
  };
  

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.REGISTER_START: return registerStart( state );
        case actionTypes.REGISTER_SUCCESS: return registerSuccess(state, action);
        case actionTypes.REGISTER_FAILED: return registerFailed(state, action);

        case actionTypes.LOGIN_START: return loginStart(state, action);
        case actionTypes.LOGIN_SUCCESS: return loginSuccess(state, action);
        case actionTypes.LOGIN_FAILED: return loginFailed(state, action);

        case actionTypes.CONFIRM_ACCOUNT_START: return confirmAccountStart(state, action);
        case actionTypes.CONFIRM_ACCOUNT_SUCCESS: return confirmAccountSuccess(state, action);
        case actionTypes.CONFIRM_ACCOUNT_FAILED: return confirmAccountFailed(state, action);

        case actionTypes.LOGOUT: return logout(state);
        
        case actionTypes.RESET_ERROR_MESSAGE: return resetErrorMessage(state, action);

        default: 
            return state;
    }
}

export default authReducer;