import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../utility";

const initialState = {
  employee: [],
  employees: [],
  employeesOptions: [],
  error: null,
  loading: false
};

const loadEmployeeStart = state => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const loadEmployeeSuccess = (state, action) => {
  return updateObject(state, {
    employee: action.employee,
    error: null,
    loading: false
  });
};

const loadEmployeeFailed = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};

const loadEmployeesStart = state => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const loadEmployeesSuccess = (state, action) => {
  return updateObject(state, {
    employees: action.users,
    employeesOptions: action.users.map(option => {
      return {
        value: option.emailAddress,
        label: option.firstname + " " + option.lastname
      };
    }),
    error: null,
    loading: false
  });
};

const loadEmployeesFailed = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};

const filterEmployeesStart = state => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const filterEmployeesSuccess = (state, action) => {
  return updateObject(state, {
    employees: action.users,
    error: null,
    loading: false
  });
};

const filterEmployeesFailed = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false
  });
};

const addCandidateStart = state => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const addCandidateSuccess = state => {
  return updateObject(state, {
    error: null,
    loading: false
  });
};

const addCandidateFailed = (state, action) => {
  return updateObject(state, {
    loading: false,
    error: action.error
  });
};

const acceptCandidateStart = state => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const acceptCandidateSuccess = state => {
  return updateObject(state, {
    error: null,
    loading: false
  });
};

const acceptCandidateFailed = (state, action) => {
  return updateObject(state, {
    loading: false,
    error: action.error
  });
};

const removeParticipantStart = state => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const removeParticipantSuccess = state => {
  return updateObject(state, {
    error: null,
    loading: false
  });
};

const removeParticipantFailed = (state, action) => {
  return updateObject(state, {
    loading: false,
    error: action.error
  });
};

const employeeReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOAD_EMPLOYEE_START:
      return loadEmployeeStart(state);
    case actionTypes.LOAD_EMPLOYEE_SUCCESS:
      return loadEmployeeSuccess(state, action);
    case actionTypes.LOAD_EMPLOYEE_FAILED:
      return loadEmployeeFailed(state, action);

    case actionTypes.LOAD_EMPLOYEES_START:
      return loadEmployeesStart(state);
    case actionTypes.LOAD_EMPLOYEES_SUCCESS:
      return loadEmployeesSuccess(state, action);
    case actionTypes.LOAD_EMPLOYEES_FAILED:
      return loadEmployeesFailed(state, action);

    case actionTypes.FILTER_EMPLOYEES_START:
      return filterEmployeesStart(state);
    case actionTypes.FILTER_EMPLOYEES_SUCCESS:
      return filterEmployeesSuccess(state, action);
    case actionTypes.FILTER_EMPLOYEES_FAILED:
      return filterEmployeesFailed(state, action);

    case actionTypes.ADD_CANDIDATE_START:
      return addCandidateStart(state);
    case actionTypes.ADD_CANDIDATE_SUCCESS:
      return addCandidateSuccess(state);
    case actionTypes.ADD_CANDIDATE_FAILED:
      return addCandidateFailed(state, action);

    case actionTypes.ACCEPT_CANDIDATE_START:
      return acceptCandidateStart(state);
    case actionTypes.ACCEPT_CANDIDATE_SUCCESS:
      return acceptCandidateSuccess(state);
    case actionTypes.ACCEPT_CANDIDATE_FAILED:
      return acceptCandidateFailed(state, action);

    case actionTypes.REMOVE_PARTICIPANT_START:
      return removeParticipantStart(state);
    case actionTypes.REMOVE_PARTICIPANT_SUCCESS:
      return removeParticipantSuccess(state);
    case actionTypes.REMOVE_PARTICIPANT_FAILED:
      return removeParticipantFailed(state, action);

    default:
      return state;
  }
};

export default employeeReducer;
