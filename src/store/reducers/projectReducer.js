import * as actionTypes from "../actions/actionTypes";
import { updateObject } from "../utility";

const initialState = {
  projects: [],
  project: [],
  error: null,
  loading: false,
  errorFetching: ""
};

const loadProjectsStart = state => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const loadProjectsSuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    loading: false,
    projects: [...action.projects]
  });
};

const loadProjectsFailed = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: true
  });
};

///===== ONE PROJECT =====///

const loadProjectStart = state => {
  return updateObject(state, {
    error: null,
    loading: true
  });
};

const loadProjectSuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    project: action.project,
    loading: false
  });
};

const loadProjectFailed = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: true
  });
};

const addProjectStart = state => {
  return updateObject(state, {
    error: null,
    errorFetching: "",
    loading: true
  });
};

const addProjectSuccess = (state, action) => {
  return updateObject(state, {
    error: null,
    errorFetching: "fetched",
    loading: false,
    response: action.response
  });
};

const addProjectFailed = (state, action) => {
  return updateObject(state, {
    error: action.error,
    loading: false,
    errorFetching: "fetched"
  });
};

const projectReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOAD_PROJECT_START:
      return loadProjectStart(state);
    case actionTypes.LOAD_PROJECT_SUCCESS:
      return loadProjectSuccess(state, action);
    case actionTypes.LOAD_PROJECT_FAILED:
      return loadProjectFailed(state, action);

    case actionTypes.LOAD_PROJECTS_START:
      return loadProjectsStart(state);
    case actionTypes.LOAD_PROJECTS_SUCCESS:
      return loadProjectsSuccess(state, action);
    case actionTypes.LOAD_PROJECTS_FAILED:
      return loadProjectsFailed(state, action);

    case actionTypes.ADD_PROJECT_START:
      return addProjectStart(state);
    case actionTypes.ADD_PROJECT_SUCCESS:
      return addProjectSuccess(state, action);
    case actionTypes.ADD_POSITIONS_FAILED:
      return addProjectFailed(state, action);

    default:
      return state;
  }
};

export default projectReducer;
